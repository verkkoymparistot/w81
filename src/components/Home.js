import React from "react";

const Home = (props) => {
    return (
    <>
    <div>
        <h1>
            Welcome to the Pet clinic!
        </h1>    
        <p>Click Pet Info to see all the pets or Pet Add to add a new pet.</p>
    </div>
    </>
    )
}

export default Home;