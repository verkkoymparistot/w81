import React, { useState, useEffect } from "react";
import { initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from "firebase/firestore";
import firebaseConfig from './firebaseConfig';

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const PetInfo = () => {
  const [pets, setPets] = useState([]);

  const fetchData = async () => {
    const querySnapshot = await getDocs(collection(db, 'pets'));
    const petData = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
    setPets(petData);
  };

  useEffect(() => {
    fetchData();
  }, []);

  
  return (
    <>
      <h2>Pet Information:</h2>
      <ul>
        {pets.map(pet => (
          <li key={pet.id}>{pet.name} : {pet.type}</li>
        ))}
      </ul>
    </>
  );
}

export default PetInfo;