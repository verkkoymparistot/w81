import React from 'react';
import './App.css';
import { Link, Routes, Route } from 'react-router-dom';
import Home from './components/Home';
import PetInfo from './components/PetInfo';
import PetAdd from './components/PetAdd';

const App = (props) => {
  return (
    <div className='app'>
      <header className="p-3 text-bg-dark">
        <div className="container">
          <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul className="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
              <li><Link to="/" className="nav-link px-2 text-white">Home</Link></li>
              <li><Link to="/petinfo" className="nav-link px-2 text-white">Pet Info</Link></li>
              <li><Link to="/petadd" className="nav-link px-2 text-white">Pet Add</Link></li>
              </ul>
          </div>
        </div>
      </header>

      <Routes>
        <Route path='/' element={<Home />} />
        <Route path="/petinfo" element={<PetInfo />} />
        <Route path="/petadd" element={<PetAdd />} />
      </Routes>
    </div>
  );
}

export default App;
